local Roomba = class('Roomba', Creature)

function Roomba:initialize()
    Creature.initialize(self, 'Roomba', 'roomba')
    self.sound = music.vacuum:play()
    self.sound:setVolume(0.05)
    self.imageGroup = ImageGroup:new('roomba', vector(0.5, 0.5), ImageGroup.static.eight, {} )
    self.radius = 4*6
    self.anchor = vector(24,24)
    self.logicUpdateInterval = 1000
end

function Roomba:update(dt)
    Creature.update(self, dt)
    if phase == 6 then
        self.sound:setVolume(0)
    end
end

function Roomba:logicUpdate()
    repeat
        self.movement = vector.randomDirection(self.speed)
    until self:testTileAtPixelPos(self.pos + self.movement)
end

return Roomba
-- vim: ft=lua et ts=4 sw=4 sts=4
