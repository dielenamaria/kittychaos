local AutonomousHuman = class('AutonomousHuman', Human)

function AutonomousHuman:initialize()
    Human.initialize(self)
    self.timeSinceStateChange = 0
    self:setState("away")
    self.substate = "wait"
    self.pos = vector(-124,550)
    self.shoppingCounter = 0
end

function AutonomousHuman:update(dt)
    self.timeSinceStateChange = self.timeSinceStateChange + dt
    if self.state == "pizza" and self.substate == "goout" then
        self.lookDirection = vector(0,1)
        self.pos = self.pos + vector(0, self.speed) * dt
        if self.pos.y > 450 then
            self.substate = "goin"
            self.shoppingCounter =  self.shoppingCounter + 1

            if self.shoppingCounter == 1 then
                self.cargo = Edible:new()
            elseif self.shoppingCounter == 2 then
                self.cargo = Dog:new()
                --setMusic(music.catter_house) 
            elseif self.shoppingCounter == 3 then
                self.cargo = Cat:new()
            elseif self.shoppingCounter == 4 then
                self.cargo = Roomba:new()
                --setMusic(music.cattest_house) 
            else    

                r = math.random(1, 100)
                if r < 60 then
                    self.cargo = Edible:new()
                elseif r < 80 then
                    self.cargo = Cat:new()
                elseif r < 90 then
                    self.cargo = Roomba:new()
                else
                    self.cargo = Dog:new()
                end   
            end
        end
    elseif (self.state == "pizza" or self.state == "away") and self.substate == "goin" then
        self.lookDirection = vector(0,-1)
        self.pos = self.pos - vector(0, self.speed) * dt
        if self.pos.y < 250 then
            doorOpen = false
            sounds.door_close:setVolume(0.3)
            sounds.door_close:play()
            if self.state == "pizza" then
                self.substate = "putpizza" 
                self:dropAtRandomPosition()
            else
                self:setState("wusel")
            end
        end
    elseif self.state ~= "away" then 
        Human.update(self,dt)
    else
        self:logicUpdate()
    end
end

function AutonomousHuman:handleTargetReached(target)
    if self.state == "wusel" then    
        Human.handleTargetReached(self, target)
        self:printf("Reached %s", target)
        if vector.isvector(target) then
            if math.random(1, 2) == 1 then
                self:setState("pizza")
            else
                self:fetchRandomObject()
            end
        else
            self:dropAtRandomPosition()
        end
    elseif self.state == "pizza" then
        if self.substate == "gotodoor" then
            doorOpen = true
            sounds.door_open:setVolume(0.6)
            sounds.door_open:play()
            self.substate = "goout"
        elseif self.substate == "putpizza" then
            Human.handleTargetReached(self, target)
            self:setState("wusel")
        end
    end
end

function AutonomousHuman:fetchRandomObject()
    local keyset = {}
    for k, v in pairs(objects) do
        if v ~= self then
            table.insert(keyset, k)
        end
    end
    obj = objects[keyset[math.random(#keyset)]]
    self:printf("chose: %s", obj)
    self:setTarget(obj)
end
    
function AutonomousHuman:logicUpdate()
    if self.state == "away" and self.substate == "wait" then
        if self.timeSinceStateChange > 15 then
            self:printf("WAAAAA")
            self.substate = "goin"
            sounds.door_open:setVolume(0.6)
            sounds.door_open:play()
            
            doorOpen = true
        end
    end
    if self.state == "wusel" then
        if self.targetObject == nil and self.cargo == nil then
            self:fetchRandomObject()
        end
    end
    if self.state == "pizza" then
        if self.substate == nil then
            self.substate = "gotodoor"
            self:setTarget(vector(-124, 250))
        end
    end
end

function AutonomousHuman:dropAtRandomPosition()
    pos = self:randomWalkablePosition()
    self:printf("chose: %s", pos)
    self:setTarget(pos)
    self.shallDropCargo = true
end

function AutonomousHuman:setState(newState)
    if self.state == newState then return end

    self:printf ("Changed state from %s to %s.", self.state, newState)
    
    self.state = newState
    self.substate = nil
    self.timeSinceStateChange = 0
    self:setTarget(nil)
    self.timeSinceLogicUpdate = 1000
end

return AutonomousHuman
