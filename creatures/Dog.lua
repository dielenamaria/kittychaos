local Dog = class('Dog', TargetCreature)

function Dog:initialize()
    TargetCreature.initialize(self, 'Hund')
    self.imageGroup = ImageGroup:new('dog', vector(0.5, 1), ImageGroup.static.NESW, {} )
    self:setTarget(nil)
    self.radius = 4*8
    self.speed = 90
    self.state = "chasing"
    self.defaultLogicUpdateInterval = 8
    self.logicUpdateInterval = self.defaultLogicUpdateInterval
end

function Dog:logicUpdate(dt)
    -- chase cats
    print("Dog state before: "..self.state)
    if self.state == "chasing" then
        if math.random(0,8) == 3 then
            print("Dog: going to sleep")
            self.state = "going to sleep"
            -- update() will check if the basked was reached
            self:setTarget(tileToPixelPos(DOG_BASKET_CENTER))
            self.logicUpdateInterval = 100
            return
        else
            possibleTarget = self:randomObjectOfType(Cat)
            self:setTarget(possibleTarget)
        end

    -- wake up some time
    elseif self.state == "sleeping" then
        if math.random(0,5) == 4 then
            print("Dog: wake up")
            self.state = "chasing"
            self:setTarget(self:randomObjectOfType(Cat))
        end
        return
    end

    print("Dog state after: "..self.state)
end

function Dog:selectImage()
    if self.state == "sleeping" then
        return self.imageGroup:getImage(vector(1,0), "sleeping")
    else
        return self.imageGroup:getImage(self.lookDirection)
    end
end

function Dog:update(dt)
    if self.state == "going to sleep"
        and self:distanceTo(tileToPixelPos(DOG_BASKET_CENTER)) < 5 then
        print ("Dog: sleeping")
        self.state = "sleeping"
        self.logicUpdateInterval = self.defaultLogicUpdateInterval
        self.timeSinceLogicUpdate = 0
    else
        TargetCreature.update(self, dt)
    end
    
function Dog:handlePickUp()
    sounds.bark:setVolume(1)
    sounds.bark:play()
    self.state = "chasing"
end

end

return Dog
-- vim: ft=lua et ts=4 sw=4 sts=4
