local Mouse = class('Mouse', TargetCreature)

function Mouse:initialize()
    TargetCreature.initialize(self, 'Maus')
    self.imageGroup = ImageGroup:new('mouse', vector(0.5, 1), ImageGroup.static.NESW, {} )
    self:setTarget(nil)
    self.speed = 150
    self.radius = 50
    self.copulating = false
    self:setNextCopulationTime()
end

function Mouse:update(dt)
    TargetCreature.update(self, dt)
    self.nextCopulationTime = self.nextCopulationTime - dt
end

function Mouse:setNextCopulationTime()
    self.nextCopulationTime = math.random(20,80)
end

function Mouse:tryCopulate(dt, other)
    -- don't try to copulate with two different mice at the same time
    --print (("try copulate %d %d"):format(other.nextCopulationTime, self.nextCopulationTime))
    if self.copulating or other.copulating then
        --print("not today")
        return
    end

    self.copulating = true
    other.copulating = true

    if other.nextCopulationTime < 0 and self.nextCopulationTime < 0 then
        print("Mouse: COPULATING")
        local n = Mouse:new()
        n.pos = self.pos
        table.insert(objects, n)

        self:setNextCopulationTime()
        other:setNextCopulationTime()
    end

    self.copulating = false
    other.copulating = false

end

function Mouse:logicUpdate()
    if self.targetPos == nil then
        repeat
            possibleTarget = self.pos + vector.randomDirection(30,80)
        until self:testTileAtPixelPos(possibleTarget)
        self:setTarget(possibleTarget)
    end
end

function Mouse:handlePickUp()
    sounds.mouse:setVolume(0.5)
    sounds.mouse:play()
end

return Mouse
-- vim: ft=lua et ts=4 sw=4 sts=4
