local SuperCat = class('SuperCat', Cat)

function SuperCat:initialize()
    Cat.initialize(self, "orange")
    self.instanceName = "SuperCat(TM)"
    --self.logicUpdateInterval = nil
    self.state = "standing"
    self.speed = 160
end

function SuperCat:draw()
    Cat.draw(self)
    tw = self.text:getWidth()
end

function SuperCat:update(dt)
    Cat.update(self,dt)
    if self.state == "chasing" and self.targetObject then
        dist = self:distanceTo(self:getTarget())
        if dist < 70 then
            self.speed = 400
        elseif dist < 150 then
            self.speed = 150
        else
            self.speed = 120
        end
    else
        self.speed = 120
    end
end

function SuperCat:waitingBehaviour()
    if self.timeSinceStateChange > 8 then
        self:setState("sleeping")
    elseif self:shouldBeScared() then
        self:setState("scared")
    end
end

function SuperCat:walkingBehaviour()
    if not self:getTarget() then
        self:setState("waiting")
        return
    end

    if self:shouldBeScared() then
        self:setState("scared")
    else
        --mouse = self:nearestObjectOfType(Mouse, 200)
        local target = self:getTarget()
        if target and target.isInstanceOf and target:isInstanceOf(Mouse)
            and self:distanceTo(target) < 200 then
            self:setState("chasing")
            self.targetObject = target
        end
    end
end

function SuperCat:setTarget(target)
    self:printf("set target: %s", target)

    if self.state == "sleeping" and target ~= nil then
        self.setState("walking")
        return
    end

    if self.state ~= "eating" and self.state ~= "sleeping" then
        if vector.isvector(target) then
            self:setState("walking")
        elseif target and (target:instanceOf(Mouse) or target:instanceOf(Edible)) then
            self:setState("chasing")
        else
            self:setState("walking")
        end
    end
    if self.state ~= "eating" or target == nil then
        Cat.setTarget(self, target)
    end
end

return SuperCat