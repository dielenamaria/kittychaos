Edible = class('Edible', Object)

function Edible:initialize(name, graphic, nutrition)
    if name == null then
        names = {"pizza", "cheese"}
        nutritions = {5, 3}
        index = math.random(1,#names)
        name = names[index]
        nutrition = nutritions[index]
    end
    Object.initialize(self, name)
    graphic = graphic or name
    self.nutritionalValue = nutrition
    self.imageGroup = ImageGroup:new(graphic, vector(0.5, 0.5))
    self.radius = 3*8
end

function Edible:ingest(predator)
    if predator and predator.energy then
        predator.energy = predator.energy + self.nutritionalValue * 100
    end
    if predator and predator.foodCount then
        predator.foodCount = predator.foodCount + self.nutritionalValue
    end
    removeFromFloor(self)
end

return Edible
-- vim: ft=lua et ts=4 sw=4 sts=4
